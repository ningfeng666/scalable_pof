### 主要功能

udp监听8888端口根据命令执行动作：

- 1：开启pof
- 2：开启ospf
- 3：关闭全部
- 4：退出程序

### 快速开始

```sh
# 编译程序
mkdir build
g++ ospf.cpp -o build/ospf
g++ pof.cpp -o build/pof
gcc scalable_pof.c -o build/scalable_pof
```

```
# 测试
cd build
./scalable_pof
# 新建一个终端
nc -uv 127.0.0.1 8888
# 输入1 2 3 4即可观察效果
```

**效果展示**

```
# 终端1
root@ningfeng-OptiPlex-7080:~/c_test/mult_process/build# ./scalable_pof 
POF RUNNING
POF RUNNING
POF RUNNING
POF RUNNING
OSPF RUNNING
OSPF RUNNING
OSPF RUNNING
OSPF RUNNING
root@ningfeng-OptiPlex-7080:~/c_test/mult_process/build# 

# 终端2
root@ningfeng-OptiPlex-7080:~/c_test# nc -uv 127.0.0.1 8888
1
Connection to 127.0.0.1 8888 port [udp/*] succeeded!
2
3
4
^C
root@ningfeng-OptiPlex-7080:~/c_test# 
```

