#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#define SERVER_PORT 8888
#define BUFF_LEN 100


pid_t run_cmd(char* cmd,char **arglist){
    // 新建进程执行指令
    pid_t fpid;
    fpid=fork();
    if (fpid==0){ // 子进程
        execvp(cmd,arglist);
        exit(-1); // 执行失败立即退出
    }
    return fpid;
}
int main(int argc, char** argvs){

    pid_t pof_pid=0, ospf_pid=0;
    int socket_fd;
    struct sockaddr_in ser_addr,clent_addr; 
    char buf[BUFF_LEN];
    if((socket_fd = socket(AF_INET, SOCK_DGRAM, 0))<0){
        printf("socker init error\n");
        return -1;
    } 
    memset(&ser_addr, 0, sizeof(ser_addr));
    ser_addr.sin_family = AF_INET;
    ser_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    ser_addr.sin_port = htons(SERVER_PORT); 
    if(bind(socket_fd, (struct sockaddr*)&ser_addr, sizeof(ser_addr))<0){
        printf("socker bind error\n");
        return -1;
    }
    int len = sizeof(clent_addr);
    while(1){
        memset(buf, 0, BUFF_LEN);
        int ret = recvfrom(socket_fd, buf, BUFF_LEN, 0, (struct sockaddr*)&clent_addr, &len); 
        if(ret<0){
            printf("udp erro\n");
            return -1;
        }
        switch (buf[0]){
            case '1': // 开启pof
                if (pof_pid>0)
                    break;
                if (ospf_pid>0){
                    kill(ospf_pid, SIGKILL);
                    ospf_pid = 0;
                }
                pof_pid=run_cmd("./pof",NULL);
                // pof_pid=fpid;
                break;
            case '2': // 开启ospf
                if (ospf_pid>0)
                    break;
                if (pof_pid>0){
                    kill(pof_pid, SIGKILL);
                    pof_pid = 0;
                }
                ospf_pid=run_cmd("./ospf",NULL);
                // ospf_pid=fpid;
                break;
            case '3': // 关闭所有进程
                if (ospf_pid>0){
                    kill(ospf_pid, SIGKILL);
                    ospf_pid = 0;
                }
                if (pof_pid>0){
                    kill(pof_pid, SIGKILL);
                    pof_pid = 0;
                }
                break;
            case '4': // 退出程序
                if (ospf_pid>0)
                    kill(ospf_pid, SIGKILL);
                if (pof_pid>0)
                    kill(pof_pid, SIGKILL);
                return 0;
        }
    }
    return 0;
    
}